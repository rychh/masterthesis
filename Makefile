SOURCES := $(shell find $(SOURCEDIR) -name '*.tex')

all: main.pdf

main.pdf: $(SOURCES) main.bbl 
	pdflatex main.tex
	pdflatex main.tex

main.aux: $(SOURCES)
	pdflatex main.tex

main.bbl: bib.bib main.aux
	bibtex main

clean:
	rm -f img/*.log img/*.aux 
	rm -f *~ *.log *.out *.aux *.blg *.bbl *.gz

distclean: clean
	rm -f main.pdf

tar: clean
	bash -c "cd ..; tar -czf ${BASE}.tar.gz ${BASE}/*; mv ${BASE}.tar.gz ${BASE}"


diff:
	mkdir /tmp/arora-improved/
	python2 ~/latex-flatten.py main.tex /tmp/arora-improved/test.tex
	python2 ~/latex-flatten.py /tmp/arora-improved/test.tex /tmp/arora-improved/test2.tex
	python2 ~/latex-flatten.py /tmp/arora-improved/test2.tex /tmp/arora-improved/d2.tex
	git checkout $(branch-id)
	python2 ~/latex-flatten.py main.tex /tmp/arora-improved/test.tex
	python2 ~/latex-flatten.py /tmp/arora-improved/test.tex /tmp/arora-improved/test2.tex
	python2 ~/latex-flatten.py /tmp/arora-improved/test2.tex /tmp/arora-improved/d1.tex
	latexdiff /tmp/arora-improved/d1.tex /tmp/arora-improved/d2.tex > /tmp/arora-improved/diff.tex
	pdflatex /tmp/arora-improved/diff.tex
	git checkout master
