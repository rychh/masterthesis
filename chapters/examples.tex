\chapter{Improved data structures}
\label{sec:examples}

As already mentioned in Introduction, in~\cite[Chapter~4 - Applications of Meta-Theorems]{DynamicFPT}, we show a powerful meta-theorem which
guarantees $\Oh_k(\log\log n)$ worst-case update time for dynamic versions of {\sc{Disjoint Factors}} and {\sc{Edit Distance}}.
Now, we illustrate that the dependence on $k$ can be significantly improved by exploiting combinatorial structure of this
problems. In the Section~\ref{sec:df} we give an improved dependence for
{\sc{Disjoint Factors}} and in Section~\ref{sec:edit-distance} we present a data structure for {\sc{Edit Distance}}.

\section{$k$-Disjoint Factors}
\label{sec:df}


\begin{lemma}
    \label{lem:dynamic-df}
     There is a data structure for the dynamic {\sc{Disjoint Factors}} problem
     with initialization time $\Oh(kn)$, worst-case update time $\Oh(\log\log n)$, and query time $\Oh(k 2^{k} \log\log n)$.
\end{lemma}

As a corollary of this lemma we immediately obtain $\Oh(k 2^k + kn)$ time algorithm for static version of {\sc{Disjoint Factors}} problem.
This improves an $\Oh(k 2^k n)$ time algorithm due to Bodlaender et al.~\cite{bodleander11}.

\begin{corollary}
    \label{cor:static-df}
    {\sc{Disjoint Factors}} can be statically solved in $\Oh(k 2^k + kn)$ time.
\end{corollary}
\begin{proof}[Proof of Corollary~\ref{cor:static-df}]
    We calculate the solution to the {\sc{Disjoint Factors}} with dynamic
    programming.
    The entries of the dynamic programming are parameterized by sets $S \subseteq \Sigma$.
    For each $S$, we want ${\mathtt{DF}}[S]$ to be a minimal integer $\ell \in [n]$, such that a word $w[1 : \ell]$ contain solution to $|S|$-{\sc{Disjoint Factors}} with alphabet $S$.

    We compute the entries of the dynamic programming in the bottom-up fashion.
    In the base case $S = \emptyset$ the answer is $\ell = 1$. When $S$ is nonempty, we
    guess a letter $s \in S$. Next, we determine the position of $\ell_s$ for set
    $S\setminus\{s\}$. Then, we look for the first $s$-factor after position
    $\ell_s$ in the word and return the position of its right-endpoint.
    To be more precise, the dynamic programming table $\mathtt{DF}$ is filled using the following formula:

    \begin{equation}
        \label{eq:DF}
        {\mathtt{DF}}[S] := \begin{cases}
            \min_{s \in S} \left\{ \text{next } s \text{-factor after } \mathtt{DF}[S \setminus \{s\}]\right\} & \text{if } S \neq \emptyset,\\
            1 & \text{otherwise}.
        \end{cases}
    \end{equation}

    For correctness, observe that every optimal solution to {\sc{Disjoint Factor}}
    can be represented as a permutation $\pi : [k] \rightarrow [k]$ that
    corresponds to the order in which each factor appears in the word. Moreover, if
    the solution to {\sc{Disjoint Factor}} that is represented by permutation
    $\pi$ exists, then it can be detected by greedily taking subsequent factors
    in the word (cf.,~\cite{bodleander11}).

    It remains to show that there exists a data-structure that can be
    initialized in  $\Oh(kn)$ time
    and can answer queries of the form $(s,\ell) \in \Sigma \times [n]$. The
    query returns a position of the first right-endpoint of
    $s$-factor in the word $w[\ell:n]$. To achieve this, we store the table
    $\mathtt{next}[s,\ell]$ for every $s \in \Sigma$ and $\ell \in
    [n]$. It stores the minimal position of a letter $s \in \Sigma$ in the
    word $w[\ell:n]$. To compute $\mathtt{next}[s,\ell]$ for a fixed $s
    \in \Sigma$, we fill it starting from $\ell$ equal to $n$ down to
    $1$. Initially $\mathtt{next}[s,n] = \infty$ for every $s \in
    \Sigma$. If $w[\ell] = s$, then we set $\mathtt{next}[s,\ell] = \ell$.
    Otherwise, we know that the next position of a letter $s$ is after
    $\ell$ and we set the value of the table $\mathtt{next}$ at $s$ and
    $\ell$ to $\mathtt{next}[s,\ell+1]$.

    Therefore, to find the position of next $s$-factor after position $\ell$ we lookup
    the value of $\ell' := \mathtt{next}[s,\ell]$. This means that next
    $s$-factor starts at position $\ell'$. We return the position of
    $\mathtt{next}[s,\ell']$ as the right-endpoint of this
    $s$-factor.

    Observe, that we can compute table $\mathtt{next}$ in time
    $\Oh(kn)$. Moreover, each $\mathtt{next}$ query takes $\Oh(1)$ time. Hence, with
    $\mathtt{DF}$ procedure we can solve \textsc{Disjoint
    Factor} in $\Oh(k 2^k + kn)$ time.
\end{proof}

\begin{proof}[Proof of Lemma~\ref{lem:dynamic-df}]
    In the dynamic setting, we need to show that for every $s \in
    \Sigma$ and $\ell \in [n]$ the position of a next $s$-factor after
    position $\ell$ can be found in $\Oh(\log\log n)$ time.  To achieve that,
    for every letter $s \in \Sigma$ we maintain a van Emde Boas tree $\Tt_s$.
    In the data structure, $\Tt_s$ stores the positions of
    a letter $s$ in the updated word. Note, that a single update to $\Tt_s$ can be
    done in $\Oh(\log\log n)$ time. During query, for a given $s \in \Sigma$ and $\ell
    \in [n]$ we can ask for the next position of a letter $s$ after $\ell$. A
    single query to van Emde Boas tree takes $\Oh(\log\log n)$ times.
    
    Similarly to the proof of Corollary~\ref{cor:static-df}, we can use $\Tt_s$
    to find a position of $s$-factor after a given position in
    $\Oh(\log\log n)$, with two queries to $\Tt_s$. Observe, that during an
    update to {\sc{Disjoint Factor}} we need to update only two van Emde Boas
    trees. Hence update takes $\Oh(\log\log n)$ time. Moreover, $\mathtt{DF}$
    procedure can answer queries to {\sc{Disjoint Factor}} in $\Oh(k 2^k
    \log\log n)$ time. 
\end{proof}

\section{Edit Distance}
\label{sec:edit-distance}

\newcommand{\LCE}{\mathsf{LCE}}

\begin{lemma}
    \label{lem:ed}
     There is a data structure for the dynamic {\sc{Edit Distance}} problem
     with initialization time $\Oh(kn)$, worst-case update time $\Oh(k \log\log n)$,
     and query time $\Oh(k^2 \log\log n)$.
\end{lemma}

The data structure is based on the classical result of Landau and
Vishkin~\cite{landau-vishkin} who gave a static $\Oh(n+k^2)$ time algorithm for
$k$-\textsc{Edit Distance} problem. Our simple observation is that $\LCE$-queries in
their algorithm can be efficiently maintained with van Emde Boas trees.

Before we proceed with the proof of Lemma~\ref{lem:ed} let us recall the
definition of \emph{Longest Common Extension} query. Let $x,y$ be two strings of
length at most $n$. We define $\LCE(i,j)$ as the largest integer $\ell \in [n]$,
such that $x[i:i+\ell] = y[j:j+\ell]$.  We show, that with van Emde Boas data
structure, we can efficiently maintain an answer to the $\LCE$ queries when
$|i-j|$ is small.

\begin{proposition}[Dynamic $\LCE$-queries]
    \label{prop:lce}
    For any $k \in \nat$, there is a data structure that maintains words $x,y \in \Sigma^n$ and
    supports the following operations:
    \begin{itemize}
        \item $\init(x,y)$: Initialize the data structure with the given words $x,y$; takes $\Oh(kn)$ time,
        \item $\update(w,i,s)$: Update a word $w \in \{x,y\}$ by replacing the symbol
            at position $i$ with $s\in \Sigma$; takes  $\Oh(k \log\log n)$ time,
        \item $\query(i,j)$: if $|i-j| \le k$ return $\LCE(i,j)$ of $x,y$; takes  $\Oh(\log\log n)$ time.
    \end{itemize}
\end{proposition}

\begin{proof} 
    For every fixed $p \in \{-k,\ldots,k\}$ we maintain the van Emde
    Boas data-structure $\Tt_p$. Each $\Tt_p$ maintains a set $S_p
    \subseteq [n]$ defined as:

    \begin{displaymath}
        S_p := \{ i \in [n] \text{ such that } x[i] \neq y[i+p]\}
        .
    \end{displaymath}
    
    Observe, that sets $S_p$ can be efficiently maintained with van Emde Boas
    data structures. During each update to word $x[i]$ we iterate over
    every $p \in \{-k,\ldots,k\}$, check if $x[i] \neq y[i+p]$ and update
    $\Tt_p$ accordingly. Single query to $\LCE$ can be done with a single query
    to $\Tt_p$.
\end{proof}

With that data-structure to answer $\LCE$-queries under text updates we can proceed with
the description of the dynamic data structure for $k$-\textsc{Edit Distance} with
$\Oh(k \log\log n)$ worst-case update time.

\begin{proof}[Proof of Lemma~\ref{lem:ed}]

    When length of $x$ and $y$ differ by more than $k$ then the edit distance
    between $x$ and $y$ must be greater than $k$ and we can conclude that the
    answer is negative.  Our algorithm is based on the static algorithm of
    Landau and Vishkin~\cite{landau-vishkin}. They consider the dynamic
    programming table $\mathtt{D}[i,j] := \mathtt{ed}(x[1:i],y[1:j])$ for edit
    distance. They claim that it suffices to touch only $\Oh(k^2)$ entries of
    the table $\mathtt{D}$ in order to retrieve $\mathtt{D}[|x|,|y|]$ (if its
    value is $\Oh(k)$).
    The main observation of the static $\Oh(n+k^2)$ time algorithm of Landau and
    Vishkin~\cite{landau-vishkin} is that $\mathtt{D}[i+1,j+1] \in
    \{\mathtt{D}[i,j], \mathtt{D}[i,j]+1\}$. Therefore, it is always beneficial
    to greedily take $\LCE$ to compute the next value in the table $\mathtt{D}$.
    Because we assumed that the edit distance between $x$ and $y$ is at most
    $k$, we need to perform at most $k$ queries to $\LCE$. Note that we consider
    only values $\mathtt{D}[i,i+p]$ for $p \in \{-k,\ldots,k\}$. Hence we need
    to touch at most $2k+1$ entries with value exactly $\ell$ for every $\ell
    \in [k]$. In total number of entries in table $\mathtt{D}$ touched by Landau
    and Vishkin algorithm is $\Oh(k^2)$.

    \begin{equation}
        \label{eq:ED}
        \begin{aligned}
            &{\mathtt{ED}}[0, 0] := LCA(0,0)\\
            &{\mathtt{ED}}[p, l+1] := -\infty \text{ if $|p| > k$ or $l\geq k$}\\
            &{\mathtt{ED}}[p, l+1] := LCA(i,i+p) \text{ where } i = \max ({\mathtt{ED}}[p,l] + 1,{\mathtt{ED}}[p-1,l] + 1,{\mathtt{ED}}[p+1,l])\\
        \end{aligned}
    \end{equation}

    Dynamic setting for this algorithm can be represented by Equation~\ref{eq:ED}, where $i = {\mathtt{ED}}[p, l]$ and $i \neq - \infty$  is the largest due to the equation $\mathtt{ed}(x[1:i],y[1:i+p]]) = l$.
    Note that $\mathtt{ed}(x,y) \leq k$ occurs if and only if ${\mathtt{ED}}[|x|-|y|, k] \leq |x|$.
    We keep track of data structures from Proposition~\ref{prop:lce} to emulate $\LCE$ queries.
    Single update results in $\Oh(k \log\log n )$ update time.
    Single query to \textsc{Edit Distance} problem takes $\Oh(k^2 \log\log n)$ time because we perform $\Oh(k^2)$ queries to $\LCE$ (each can be done in $\Oh(\log\log n )$ time with a data structure from Proposition~\ref{prop:lce}).
\end{proof}
