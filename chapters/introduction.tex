% todo Czym jest parametryzacja?

The field of parameterized complexity is based on the principle of {\em{parameterization}}: measuring the usage of resources not only in terms of the total input size, but also in terms of auxiliary complexity measures called {\em{parameters}}. Traditionally, this principle is applied to static algorithms and their running times, but the idea can be --- and has been --- used within essentially every algorithmic paradigm. Among these, a recent line of research has identified the area of dynamic data structures as one where the application of the parameterized approach leads to new and interesting results, see e.g.~\cite{AlmanMW20,ChenCDFHNPPSWZ21,DvorakKT14,DvorakT13,GrezMPPR21,MajewskiPS21}. In this work, we continue this promising direction by investigating classic string problems considered in parameterized complexity.

We study here only decision problems, i.e., the problems to which the answer is either {{\em{yes}} or {\em{no}}.
For the problem  {\sc{P}}, we aim to provide a data structure with the following functionalities:
\begin{itemize}
 \item $\init(input)$ - initialises the data structure for a given instance of {\sc{P}}.
 \item $\update(position, letter)$ - modifies a single letter in a single word in a {\sc{P}} instance.
 \item $\query()$ - returns if the current instance is a {\em{yes}}-instance of {\sc{P}}.
\end{itemize}
In this work, we only allow modifications to the letters in words.
We assume that the number of words, the length of words, and the alphabet are invariant.
%As part of the query, we will not expect to return proof of the solution to the problem {\sc{P}} in a given instance and instead will only return the answer {\em{yes}} or {\em{no}}.

% todo Przedstawienie Closest Stringa

Arguably, the most widely known parameterized string problem is {\sc{Closest String}}.

\defproblem{{\sc{Closest String}}}{Integer $d$ and words $s_1,s_2,\ldots,s_n\in \Sigma^L$ over an alphabet $\Sigma$, each of length $L$}{Decide whether there exists a word $c\in \Sigma^L$ such that for every $i\in \{1,\ldots,n\}$, the Hamming distance between $s_i$ and $c$ is at most $d$.}

 {\sc{Closest String}} has several natural parameters: $n$, $d$, $L$, $|\Sigma|$. For the parameterization by $d$ and $|\Sigma|$, Gramm et al.~\cite{closest-string-03} gave a $d^{\Oh(d)}\cdot (nL)^{\Oh(1)}$-time algorithm, while Ma and Sun~\cite{MaS09} gave a $|\Sigma|^{\Oh(d)}\cdot (nL)^{\Oh(1)}$-time algorithm.
By now, these are textbook examples of the technique of branching~\cite[Theorem~3.14 and Exercise~3.25]{the-book}, and their running times are known to be asymptotically optimal under the Exponential Time Hypothesis (ETH)~\cite{LokshtanovMS18}.
For the parameterization by $n$, the classic algorithm of Gramm et al.~\cite{closest-string-03} solves the problem in time $2^{n^{\Oh(n)}}\cdot L^{\Oh(1)}$ by a reduction to integer programming in dimension $n^{\Oh(n)}$. Recently, Kouteck\'y et al.~\cite{KnopKM20} improved this running time to $n^{\Oh(n^2)}\cdot L^{\Oh(1)}$ using exciting developments in parameterized algorithms for block-structured integer programs.
Kernelization algorithms for {\sc{Closest String}} were studied in~\cite{BasavarajuP0R018}.

% Wyniki Closest Stringa

We provide randomized dynamic data structures whose update time matches the parametric factors in the runtimes of the algorithms of Gramm et al.~\cite{closest-string-03}.

\begin{theorem}\label{thm:ClosestString-main}
 The dynamic variant of {\sc{Closest String}} admits a randomized data structure
 with initialization time $2^{\Oh(d)}\cdot nL$, amortized update time $2^{\Oh(d)}$, and
 worst-case query time $d^{\Oh(d)}$.
 The answer to each query may result in a false positive with
 probability at most $2^{-\Omega(d)}$; there are no false negatives.
\end{theorem}

In the proof of Theorem~\ref{thm:ClosestString-main} we combine the classic
approach to {\sc{Closest String}}, originating
in~\cite{closest-string-03}, with an interesting application of the
color-coding technique, see e.g.~\cite{alon_1995_colorcoding}.
The randomization comes from the color-coding technique; we can dispose of
it using standard derandomization techniques, see e.g.~\cite[Section~5.6]{the-book}, at the cost of introducing an additional $\Oh(\log (nL))$ factor in the update time. Also, note that by the results of~\cite{LokshtanovMS18}, under ETH one cannot expect to improve the query time to $d^{o(d)}$, even in the amortized~sense.
In~\cite{DynamicFPT}, we obtain a structure with $2^{\Oh(d)}\cdot nL|\Sigma|^{1+o(1)}$ initialization time, $|\Sigma|^{\Oh(d)}$ query time and amortized $2^{\Oh(d)}$ update time, using results from Ma and Sun~\cite{MaS09}.

\medskip

% Przedstawienie DF i ED

We further investigate other string problems.

\defproblem{{\sc{Disjoint Factors}}}{A word $w \in \{1,\ldots,k\}^{\star}$, where $k$ is an integer}{Decide whether there exists pairwise disjoint subwords $w_{1}, w_{2}, \dots , w_{k}$ of $w$ such that for each $i\in \{1,\ldots,k\}$, $w_i$ has length at least $2$ and begins and ends with symbol $i$.}

\defproblem{{\sc{Edit Distance}}}{Integer $k$ and two words $u,v\in \Sigma^\star$, where $\Sigma$ is an alphabet}{Decide whether $\ED(u, v) \leq k$, that is, whether $v$ can be obtained from $u$ by a sequence of at most $k$ {\em{edits}}, each consisting of a deletion, insertion, or substitution of a single symbol.}

{\sc{Disjoint Factors}} has been introduced in~\cite{BodlaenderTY11} as a stepping stone for kernelization hardness of the {\sc{Disjoint Cycles}} and {\sc{Disjoint Paths}} problems.
This is not a known problem, but it is a good example of the techniques we have used.
Another problem we analyse is {\sc{Edit Distance}}.
This problem is known, studied and has applications in bioinformatics among others, see e.g.~\cite{shao_2012_approximating,rani_2018_enhancing,johnson_2008_a}.
It can be solved in $\Oh(n^2)$, where $n$ is the word length,
and under the Strong ETH, there is no strongly subquadratic algorithm~\cite{BackursI18,edit-distance-lb2,edit-distance-lb3,edit-distance-lb4}.
The current best algorithm runs in time $\Oh(n^2/\log(n)^2)$ time~\cite{best-edit-distance}.
The parameterized  {\sc{Edit Distance}} can be solved in time $\Oh(n+k^2)$ by the celebrated Landau and Vishkin algorithm~\cite{landau-vishkin} and even in a sublinear time when approximation is allowed~\cite{batu,AndoniN20,GoldenbergKKS21}.

%  Nasze wyniki z DF i ED (structury)
It is worth mentioning that, unlike my thesis, in~\cite{DynamicFPT} we use meta-theorems of~\cite{FrandsenMS97, McNaughtonP71,Schutzenberger65a} to show data structures for {\sc{Disjoint Factors}} and {\sc{Edit Distance}}, which both can be initialized in time $\Oh_k(n)$, and both admit the worst case update time of $\Oh_k(\log \log n)$ and constant query time.
Unfortunately, as is often the case with theory, the exact parameter dependencies are not obvious or too large.
For this reason, we also design explicit data structures for both problems.

\begin{theorem}\label{thm:DF-intro}
 The dynamic variant of {\sc{Disjoint Factors}} admits a data structure with initialization time $\Oh(k 2^k + kn)$, worst-case update time $\Oh(\log\log n)$, and worst-case query time $\Oh(k2^k \log \log n)$.
\end{theorem}

\begin{theorem}\label{thm:ED-intro}
 The dynamic variant of {\sc{Edit Distance}} admits a data structure with initialization time $\Oh(kn)$, worst-case update time $\Oh(k \log\log n)$, and worst-case query time $\Oh(k^2 \log \log n)$.
\end{theorem}

The above theorems are based on the  Lemmas~\ref{lem:dynamic-df} and~\ref{lem:ed} from Chapter~\ref{sec:examples}.
These structures use static algorithms and van Emde Boas trees~\cite{van-emde-boas}.
In both Theorems~\ref{thm:DF-intro} and ~\ref{thm:ED-intro}, constant query time can be obtained by running {\em{Query()}} after each {\em{Update(...)}}.
We do not know of any previous work analysing dynamic {\sc{Disjoint Factors}}.
However, {\sc{Edit Distance}} was considered in the dynamic setting for unbounded values of $k$ and only polynomial in $n$ updates are known~\cite{ded1,ded2,ded3}.

% Nasze wyniki z DF i ED (lower boundy)

Finally, we observe that we can use the hardness methodology proposed by Amarilli et al.~\cite{AmarilliJP21} to establish conditional lower bounds against improving the update time in Theorems~\ref{thm:DF-intro} and~\ref{thm:ED-intro}.
In other words, we have shown that unless the Conjecture~\ref{conj:pu} from the work of Amarilli et al.~\cite{AmarilliJP21} fails, there is no data structure for dynamic variants of {\sc{Disjoint Factors}} and {\sc{Edit Distance}}, where update and query operate in amortized constant time.

%  Co w jakiej kolejności będziemy mówić

\paragraph*{Organization}

In Chapter~\ref{sec:prelim} we give short preliminaries.
Next, in Chapter~\ref{sec:closest-string} we present a proof of
Theorem~\ref{thm:ClosestString-main}.
Subsequently, in Chapter~\ref{sec:examples} we give improved dynamic data structures for {\sc{Disjoint Factors}} and {\sc{Edit Distance}}
and in Chapter~\ref{sec:lower-bounds} we show lower bounds for them.
In Appendix~\ref{ch:contribution} I show my contribution to~\cite{DynamicFPT}.

% First, FPT algorithm for Closest String was given by~\cite{closest-string-03}.
% 
% van Emde Boass tree
% 
% There exist structure called EBT, which allows the following operations
% 
% -add(x) - adds $x$ to structure in $\Oh( \log \log {n} )$ time
% 
% -remove(x) - removes $x$ from structure in $\Oh( \log \log {n} )$ time
% 
% -successor(x) - returns smallest $y$, such that $y>x$ or $\infty$ otherwise in $\Oh( \log \log {n} )$ time
% 
% We assume that $n$ is current size of structure.
% 
