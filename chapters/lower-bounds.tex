\chapter{Lower bounds}
\label{sec:lower-bounds}

We start this section by defining \PU problem, which the basis for our lower bounds.

\defproblem{\PU}{Maintain a set $S \subseteq [n]$}{For a given $x \in [n]$, decide whether exists $y \in S$ such that $y < x$.}

Note that it is similar to the \textsc{Predecessor} problem, only instead of asking for the maximal $y$ we ask if there is any $y$.

The \PU problem can be solved in deterministic $\Oh(\log\log n )$ time
with a predecessor search~\cite{van-emde-boas,PatrascuT14} or in expected
$\Oh(\sqrt{\log\log{n}})$ time if randomization is allowed~\cite{prefix-u1-alg1,prefix-u1-alg2}
(Here, by time we mean the worse of the update and query times).
Unfortunately, no unconditional lower bounds is known for \PU.

Amarilli et al.~\cite{AmarilliJP21} presented a large class of problems that are at least as hard as the \PU problem.
Moreover, they showed that there are problems equivalent to \PU.
These led them to conjecture that there is no data structure for \PU that offers updates and queries in $\Oh(1)$.

\begin{conjecture}[\cite{AmarilliJP21}]
    \label{conj:pu}
    There is no data structure for \PU that achieves $\Oh(1)$ amortized time for updates and queries in the word-RAM model.
\end{conjecture}

For large enough word-size $w$ the complexity of even harder {\sc{Predecessor}} is $\Oh(1)$ in the word-RAM model~\cite{FredmanW93}.
However, for general $w$ and $n$, there are tight lower bounds~\cite{PatrascuT14}.
In Conjecture~\ref{conj:pu} we just expect that the complexity of \PU cannot be $\Oh(1)$ for general $w$ and~$n$.

We say that a problem ${\sc{P}}$ is {\em{\PU-hard}} when there is amortized constant reduction for update and query operation, from {\em{\PU} to ${\sc{P}}$ .
In this chapter we show that the dynamic versions of \textsc{Disjoint Factors} and of \textsc{Edit Distance} are \PU-hard.


\section{Lower bound for \textsc{Disjoint Factors}}

Now we prove that the $k$-\textsc{Disjoint Factors} problem is \PU-hard for $k \geq 3$.
We also show that $2$-\textsc{Disjoint Factors} admits a data structure with operations taking $\Oh(1)$ time.

\begin{lemma}
    Unless Conjecture~\ref{conj:pu} fails, there does not exist a data structure for the dynamic variant of \textsc{Disjoint Factors} for $k \ge 3$ with $\Oh(1)$ amortized
    update and query time.
\end{lemma}
\begin{proof}
    We show reduction from \PU to $3$-\textsc{Disjoint Factors}, where $\Sigma \coloneqq \{\mathtt{0,1,\#\}}$.
    Assume that $S \subseteq [n]$ is the set maintained in the \PU problem.
    Based on $S$ we construct a word:
    \begin{displaymath}
        w(S) := \mathtt{1} \; \alpha_1\; \alpha_2 \; \ldots \;\alpha_n \; \mathtt{0\;\#\;0\;0},
    \end{displaymath}
    where $\alpha_i$ is $\mathtt{1}$ if $i \in S$ and $\mathtt{0}$ otherwise.
    We will use \textsc{Disjoint Factors} data structure to maintain word $w(S)$.
    When we receive a request to add or remove a number from $S$ we will update the corresponding place in $w(S)$.
    For a threshold query to \PU of form $i \in [n]$ we do the following:
    \begin{itemize}
        \item Temporarily change the letter at position $(i+2)$ of $w(S)$ to $\mathtt{\#}$.
        \item Query the data structure for \textsc{Disjoint Factors} to decide whether the current word is a positive instance, and return this answer as the answer to the query.
        \item Finally, revert the word to the original $w(S)$.
    \end{itemize}
    We have shown how to perform all the operations from our reduction.
    Observe, that each step of that reduction can be implemented in $\Oh(1)$ time.

    To prove the correctness let us consider the word $w'(S)$ that \textsc{Disjoint Factors} structure
    stores before preforming the query (when $i < n$).
    \begin{displaymath}
        \mathtt{1} \; \alpha_1  \ldots \alpha_i \; \mathtt{\#} \; \alpha_{i+2} \ldots
        \alpha_n \; \mathtt{0\;\#\;0\;0}.
    \end{displaymath}
    In the resulting word, there are only two occurrences of letter $\mathtt{\#}$.
    Therefore, if the answer to \textsc{Disjoint Factors} is positive, then the word
    $\mathtt{\#} \alpha_{i+2} \ldots \alpha_n \mathtt{0\#}$ is a $\mathtt{\#}$-factor.
    Without loss of generality, we can assume that the last two letters $\mathtt{00}$ create a $\mathtt{0}$-factor.
    We are left to consider whether there is a $\mathtt{1}$-factor in the prefix of the word $w'(S)$,
    which is equivalent to asking whether at least one of $\alpha_1  \ldots \alpha_i$ is $\mathtt{1}$.
    This is possible only if $[i] \cap S \neq \emptyset$, which proves the correctness of our reduction.
\end{proof}

Now we show that \textsc{Disjoint Factors} for $k=2$ can be maintained in $\Oh(1)$ time.
We will start by proving a simple lemma which shows that if we had a lot of different letters then this problem becomes trivial.

\begin{lemma}
    \label{lemma:k1color}
    For any word $w \in \Sigma^\star$, if every letter in $w$ occurs more than $|\Sigma|$ times,
    then the answer to \textsc{Disjoint Factors} on $w$ is positive.
\end{lemma}

\begin{proof}
    Let $k := |\Sigma|$ and $w \in \Sigma^n$ be a word were each letter appears at least $k+1$ times.
    We prove the statement by induction on $k \ge 1$.
    In the base case $k=1$ the statement trivially holds.
    Let $i$ be the maximal index such that no letter is repeated in the subword $w[1:i]$.
    It follows that in $w[i+1:n]$ each letter occurs at least $k$ times.
    By the maximality of $i$ the letter $\alpha := w[i+1]$ occurs twice in $w[1:i+1]$.
    Let us greedily take these two occurrences of $\alpha$ as an $\alpha$-factor.

    Consider the word $w' = w[i+1:n]$ and remove all occurrences of $\alpha$ from $w'$.
    This word contains $k-1$ letters and each letter occurs at least $k$ times.
    Therefore, by the induction assumption $w'$ contains $k-1$ disjoint factors.
    By combining them with our $\alpha$-factor, we see that the original word $w$ contains $k$ disjoint factors.
\end{proof}

All that remains is to show the structure for \textsc{Disjoint Factors} for the binary alphabet.

\begin{lemma}
    There exists a data-structure for the dynamic variant of \textsc{Disjoint Factors} for $k=2$  that supports updates and queries in time $\Oh(1)$.
\end{lemma}
\begin{proof}
    Let $w \in \Sigma^n$, where  $\Sigma=\{\mathtt{0},\mathtt{1}\}$.
    We consider three cases depending on the letter distributions in the word $w$.
    If any letter occurs less than twice then the answer to $w$ must be negative.
    On the other hand, if each letter occurs more than 2 times then by the Lemma~\ref{lemma:k1color} the answer is positive.

    It remains to consider the situation when one of the letters occurs exactly twice.
    Without loss of generality let $\mathtt{1}$ be the letter that occurs twice in $w$ on positions $i,j \in [n]$.
    Therefore, we need to take these positions to construct a $\mathtt{1}$-factor.
    If an answer to
    \textsc{Disjoint Factors} is positive then the $\mathtt{0}$-factor must be
    contained in either $w[1,i-1]$ or $w[j+1:n]$.
    Both of these words contain only the letter $\mathtt{0}$, therefore if one of them is at least of size 2,
    we can construct a $\mathtt{0}$-factor.
    Hence the answer to \textsc{Disjoint Factors}
    is negative if and only if $i \le 2$ and $j \ge n-1$.

    It is therefore sufficient for us to maintain information about the first two and last two letters and the number of occurrences of the letters.
    Storage and verification procedure can be easily achieved in $\Oh(1)$ update/query time.
\end{proof}


\section{Lower bound for \textsc{Edit Distance}}

Next, we prove a lower bound for $k$-\textsc{Edit Distance} problem for $k \geq 2$.
In this section we consider words over alphabet $\Sigma=\{\mathtt{0,1,\#}\}$.
Before we proceed let us consider a simple gadget.

\begin{claim}
    \label{claim:gadget}
    Consider any word $w \in \{\mathtt{0,1}\}^n$.
    Then the edit distance between words
    \begin{displaymath}
        \mathtt{0\#0}\, w\, \mathtt{\#0\#}\qquad \text{ and }\qquad \mathtt{\#0\#}\, w\, \mathtt{0\#0}
    \end{displaymath}
    is equal to $2$ if $w = \mathtt{0}^n$.
    Otherwise, if $w$ contains any letter $\mathtt{1}$, then the edit distance is strictly greater than $2$.
    \label{lemma:1b1Wb1b}
\end{claim}

We postpone the proof of Claim~\ref{claim:gadget} for a moment.
Let us now use it to prove the \PU-hardness of \textsc{Edit Distance} for distances at least $2$.

\begin{lemma}
    Unless Conjecture~\ref{conj:pu} fails,
    there does not exist a data structure for the dynamic variant of \textsc{Edit Distance} for $k \ge 2$ that supports updates and queries in amortized time $\Oh(1)$.
\end{lemma}

\begin{proof}
    We will show reductions from \PU to $2$-\textsc{Edit Distance}, where $\Sigma \coloneqq \{\mathtt{0,1,\#\}}$.
    Assume that $S \subseteq [n]$ is the set maintained in the \PU problem.
    Based on $S$ we construct two words :
    \begin{gather*}
        x_S := \,\mathtt{0\,\#\,0}\, \alpha_1\,\ldots\,\alpha_n \,\mathtt{0\,0\,0} \\
        y_S := \,\mathtt{\#\,0\,\#}\, \alpha_1\,\ldots\,\alpha_n \,\mathtt{0\,0\,0}
    \end{gather*}
    where $\alpha_i$ is $\mathtt{1}$ if $i \in S$ and $\mathtt{0}$ otherwise.
    We will use \textsc{Edit Distance} data structure to maintain words $x_S$ and $y_S$.
    When we receive a request to add or remove a number from $S$ we will update the corresponding places in  $x_S$ and $y_S$.
    For a threshold query to \PU of form $i \in [n]$ we do the following:
    \begin{itemize}
        \item Temporarily replace letters in $x_S[i+3:i+5]$ with $\mathtt{\#0\#}$ and in $y_S[i+3:i+5]$ with $\mathtt{0\#0}$.
        \item Query the data structure for \textsc{Edit Distance} to decide whether distance between two words is at most $2$, and return negated answer as the answer to the query.
        \item Finally, revert words to the original $x_S$ and $y_S$.
    \end{itemize}
    We have shown how to perform all the operations from our reduction.
    Observe, that each step of that reduction can be implemented in $\Oh(1)$ time

    To prove the correctness let us consider words $x_S'$ and $y_S'$ that \textsc{Edit Distance} structure
    stores before preforming the query (when $i < n-3$).
    \begin{gather*}
        x_S' := \,\mathtt{0\,\#\,0}\, \alpha_1\,\ldots\,\alpha_i\, \mathtt{\#\,0\,\#} \, \alpha_{i+4}\,\ldots\,\alpha_n \mathtt{0\,0\,0} \\
        y_S' := \,\mathtt{\#\,0\,\#}\, \alpha_1\,\ldots\,\alpha_i\, \mathtt{0\,\#\,0} \, \alpha_{i+4}\,\ldots\,\alpha_n \mathtt{0\,0\,0}
    \end{gather*}
    For the correctness, observe that during a query, words $x_S'$ and $y_S'$ have following form:
    $\mathtt{0\#0}\, w\, \mathtt{\#0\#}\, v$ and $\mathtt{\#0\#}\, w\, \mathtt{0\#0}\, v$, for some $w,v \in \{\mathtt{0,1}\}^\star$.
    By Claim~\ref{claim:gadget} the edit distance between these two words is $2$ if and only if $w \in \mathtt{0}^\star$.
    This happens if and only if $S \cap [i] = \emptyset$, which concludes the correctness proof.
\end{proof}

The remaining proof of Claim~\ref{claim:gadget} can be shown by a diligent case analysis.

\begin{proof}[Proof of Claim~\ref{claim:gadget}]
    Observe that the edit distance between $\mathtt{0\#0}\, w\, \mathtt{\#0\#}$ and  $\mathtt{\#0\#}\, w\, \mathtt{0\#0}$ is at least $2$.
    Assume the contrary, i.e, that the edit distance between them is 1.
    The only one edit operation must be neither deletion nor insertion, as the words have equal length.
    One substitution is also not sufficient, as the Hamming distance between words is $6$.

    Let us now assume that the distance between these words is equal to $2$.
    We consider possible edit sequences:
    \begin{itemize}
        \item Two substitutions:

        The Hamming distance between words is $6$, so two substitutions are not sufficient.

        \item Deletion and then insertion:

        The words differ at the first position, so we need to delete the first letter $\mathtt{0}$.
        After deletion words differ at position $3$, because $w$ does not contain $\mathtt{\#}$, so we have to insert the letter $\mathtt{\#}$.
        We can no longer perform any operation, and the words are still different.

        \item Insertion and then deletion:

        We have to insert the letter $\mathtt{\#}$ in the first position because the words differ there.
        Currently, the first word contains more letters $\mathtt{\#}$ than the second, so we need to remove any of the letters $\mathtt{\#}$.
        Note that $w$ does not contain  $\mathtt{\#}$, so we need to remove  $\mathtt{\#}$ from the last position.
        Our words after this operation are as follows:
        \begin{gather*}
            \mathtt{\#\,0\,\#\,0}\, w[1]\,\ldots\,w[n-1]\,w[n] \mathtt{0\,\#}\\
            \mathtt{\#\,0\,\#}\, w[1]\, w[2]\,\ldots\, w[n] \mathtt{0\,\#\,0}
        \end{gather*}
        If these words are identical then the edit distance between the original words is equal to $2$, and this occurs if and only if $w = \mathtt{0}^n$.
    \end{itemize}

\end{proof}
