\newcommand{\init}{\mathsf{init}}
\newcommand{\update}{\mathsf{update}}
\newcommand{\query}{\mathsf{query}}

\chapter{Applications of Meta-Theorems}
\label{sec:meta-theorem}

In this chapter we first state a meta-theorem for string problems definable in first-order logic $\FO$; this result follows easily from the work of Frandsen et al.~\cite{FrandsenMS97} using the classic Sch\"utzenberger-McNaughton-Papert theorem~\cite{McNaughtonP71,Schutzenberger65a}. Then we explain how to use the meta-theorem for the following toy problems: {\sc{Disjoint Factors}} and {\sc{Edit Distance}}. As usual with meta-theorems, the parametric factor in the complexity guarantees of the obtained data structures is not explicit, and typically is much higher than if one constructs the data structure ``by hand''. Therefore, we next show how to derive concrete data structures with concrete complexity guarantees for {\sc{Disjoint Factors}} and {\sc{Edit Distance}}. Finally, we discuss a methodology for lower bounds introduced by Amarilli et al.~\cite{AmarilliJP21}, and we apply it to derive lower bounds for those two problems.

\section{A meta-theorem}

We first need to recall basic knowledge on different equivalent views on regular languages. This material is standard in the area of algebraic theory of languages, so we refer an interested reader to the book of Boja\'nczyk~\cite{Bojanczyk20} for a broader introduction. In particular, we explain the contemporary understanding of the material, and for appropriate references and historical remarks, we refer to~\cite{Bojanczyk20}.

The first view is through the lens of logic.
Fix a finite alphabet $\Sigma$. We consider the logic $\MSO[\Sigma,<]$ operating on words. In this logic there are variables for single positions (denoted with small letters) and subsets of positions (denoted with capital letters). The atomic formulas are of the following form:
\begin{itemize}
 \item equality test $x=y$;
 \item test $x\in X$ checking that position $x$ belongs to position subset $X$;
 \item for every $a\in \Sigma$, test $a(x)$ checking that at position $x$ there is symbol $a$; and
 \item test $x<y$ checking that position $x$ appears before position $y$.
\end{itemize}
Formulas of $\MSO[\Sigma,<]$ can be obtained from atomic formulas using standard boolean connectives and quantification (both universal and existential, and applicable to both types of variables). $\FO[\Sigma,<]$ is a fragment of $\MSO[\Sigma,<]$ where we disallow variables for subsets of positions.

A {\em{sentence}} is a formula without free variables. By $w\models \phi$ we mean that the sentence $\phi$ is satisfied in the word $w$. For a sentence $\phi\in \MSO[\Sigma,<]$, the language {\em{defined}} by $\phi$ consists of all words $w$ in which $\phi$ is satisfied. A language $L\subseteq \Sigma^\star$ is {\em{$\MSO$-definable}} if $L$ is defined by some $\phi\in \MSO[\Sigma,<]$ as above, and {\em{$\FO$-definable}} if it is defined by some $\phi\in \FO[\Sigma,<]$. It appears that regular languages exactly coincide with ones definable in $\MSO$.

\begin{theorem}\label{thm:mso}
 A language of finite words over a finite alphabet is regular if and only if it is $\MSO$-definable.
\end{theorem}

The next view is through semigroup homomorphisms. Consider a language $L\subseteq \Sigma^\star$. By endowing $\Sigma^\star$ with the concatenation operation we can regard it as a semigroup. For another semigroup $S$ and a (semigroup) homomorphism $h\colon \Sigma^\star\to S$, we say that $h$ {\em{recognizes}} $L$ if there exists $A\subseteq S$ such that $L=h^{-1}(A)$; in other words, whether $w\in L$ can be recognized by looking at $h(w)$ and determining whether it belongs to $A$. It turns out that regular languages are also exactly those that are recognized by homomorphisms to finite semigroups.

\begin{theorem}\label{thm:finite-state}
 A language of finite words over a finite alphabet is regular if and only if it is recognized by a homomorphism to a finite semigroup.
\end{theorem}

Further, it is known that if $L$ is regular, then there exists a unique minimal --- in terms of cardinality --- semigroup $S$ such that there is a homomorphism from $\Sigma^\star$ to $S$ recognizing $L$. This semigroup is called the {\em{syntactic semigroup}} for $S$.

It turns out that $\FO$-definable languages can be characterized in terms of algebraic properties of their syntactic semigroups. Here, a semigroup is {\em{aperiodic}} (or {\em{group-free}}) if it does not contain any non-trivial group.

\begin{theorem}[Sch\"utzenberger-McNaughton-Papert Theorem,~\cite{McNaughtonP71,Schutzenberger65a}]\label{thm:schutzeberger}
 A regular language $L$ is $\FO$-definable if and only if its syntactic semigroup is aperiodic.
\end{theorem}

With these standard tools recalled, we can proceed to the setting of dynamic data structures.

\medskip

Fix a finite alphabet $\Sigma$ and consider a language $L\subseteq \Sigma^\star$. The {\em{word problem}} for $L$ is to design a data structure that maintains a dynamic word $w\in \Sigma^\star$ and supports the following operations:
\begin{itemize}
 \item $\init(w)$: Initialize the data structure with the given word $w$.
 \item $\update(i,a)$: Update $w$ by replacing the symbol at position $i$ by symbol $a\in \Sigma$.
 \item $\query()$: Determine whether $w\in L$.
\end{itemize}
The complexity guarantees of such a data structure is typically measured in terms of $n := |w|$. Note that this value is fixed upon initialization and then stays the same throughout the life of the data structure.

We can also consider the word problem for semigroups. Suppose $S$ is a semigroup. Then the word problem for $S$ is defined as above for words over $S$ (that is, words $w\in S^\star$), where query is redefined as follows: Output the (left-to-right) product of all the symbols in $w$.

Observe that the word problem for a regular language $L\subseteq \Sigma^\star$ easily reduces to the word problem for its syntactic semigroup $S$. Indeed, if $h\colon \Sigma^\star\to S$ is the homomorphism recognizing $L$, say $L=h^{-1}(A)$ for some $A\subseteq S$, then in the reduction we can map symbols $a\in \Sigma$ to their images $h(a)\in S$, and whether $w\in L$ can be deduced by checking whether $h(w)\in A$.

Frandsen et al.~\cite{FrandsenMS97} proposed an efficient dynamic data structure for the word problem in aperiodic semigroups.

\begin{theorem}[\cite{FrandsenMS97}]\label{thm:miltersen}
 Let $S$ be a finite aperiodic semigroup. Then there is a data structure for the word problem for $S$ with initialization time $\Oh(n)$, worst-case update time $\Oh(\log \log n)$, and worst-case query time $\Oh(1)$.
\end{theorem}

By combining Theorems~\ref{thm:schutzeberger} and~\ref{thm:miltersen} using the reduction presented above, we obtain the following.

\begin{theorem}\label{thm:meta}
 Let $\Sigma$ be a finite alphabet and suppose $L\subseteq \Sigma^\star$ is $\FO$-definable. Then there is a data structure for the word problem for $L$ with initialization time $\Oh(n)$, worst-case update time $\Oh(\log \log n)$, and worst-case query time $\Oh(1)$.
\end{theorem}

A few remarks are in order. First, the proof of Theorem~\ref{thm:miltersen}
relies on induction on the Khron-Rhodes decomposition of the semigroup $S$,
where in each step of the induction one applies van Emde Boas
trees~\cite{van-emde-boas}. The induction has depth bounded by the size of $S$, so one can view this data structure as $\Oh(|S|)$ van Emde Boas trees stacked ``on top of each other''. Consequently, the constants hidden in the $\Oh(\cdot)$ notation in Theorem~\ref{thm:miltersen} depend on $S$, but not horribly: they are polynomial in $|S|$. However, there is a much more significant complexity blow-up hidden in Theorem~\ref{thm:finite-state}. Specifically, if a regular language $L$ is defined by an $\FO[\Sigma,<]$ sentence $\phi$, then the syntactic semigroup of $L$ has size bounded by a function of $|\phi|$, but this function is in general non-elementary --- it is basically a tower of height equal to the quantifier rank of $\phi$. This non-elementary dependence is known to be unavoidable~\cite{StockmeyerThesis}. Therefore, whenever one applies Theorem~\ref{thm:meta} in order to obtain data structures for a problem based on its description in $\FO$, one should bear in mind that the constants hidden in the $\Oh(\cdot)$ notation depend non-elementarily on the length of the description.

Second, recently Amarilli et al.~\cite{AmarilliJP21} gave a characterization of regular languages for which data structures with guarantees as in Theorem~\ref{thm:miltersen} exist. This characterization renders the tractability region to be a bit broader than just $\FO$-definability, for instance the languages ``on every even position there is symbol $a$'' or ``in total there is an even number of symbols $a$'' are not $\FO$-definable, but admit data structures for the word problem with constant update time. The characterization is expressed in algebraic terms and we could not find natural examples of parameterized string problems that would not be $\FO$-definable, but fall under the characterization. So we refrain from giving more details and point an interested reader to~\cite{AmarilliJP21} instead.

% \karol{I do not get that remark. For example the language ``Disjoint Factor''$\cap$ ``Total number of $a$ is even'' is not FO-definable and falls under their characterization}

\medskip

We now explain how to use Theorem~\ref{thm:meta} in practice on two examples: {\sc{Disjoint Factors}} and {\sc{Edit Distance}}. In each case, the task boils down to defining the problem in $\FO[\Sigma,<]$ for an appropriate alphabet $\Sigma$.
We start with {\sc{Disjoint Factors}}.

\begin{lemma}\label{lem:df-sentence}
 Let $k\in \N$ and $\Sigma_k=[k]$.
 There is a sentence $\phi_k\in \FO[\Sigma_k,<]$, computable from~$k$, such that for every $w\in \Sigma_k^\star$, $w$ is a yes-instance of {\sc{Disjoint Factors}} for parameter $k$ if and only if $w\models \phi_k$.
\end{lemma}
\begin{proof}
 In the sentence $\phi_k$, we first make a disjunction over all permutations $\pi\colon [k]\to [k]$. For each such $\pi$, we verify that there exist positions $x_1<y_1<x_2<y_2<\ldots<x_k<y_k$ such that for each $i\in [k]$, both at position $x_i$ and at $y_i$ there is symbol $\pi(i)$. It is straightforward to express this condition using an $\FO[\Sigma_k,<]$ sentence.
\end{proof}

By applying Theorem~\ref{thm:meta} to the language defined by sentence $\phi_k$ provided by Lemma~\ref{lem:df-sentence}, we obtain the following.

\begin{corollary}
 There is a data structure for the dynamic {\sc{Disjoint Factors}} problem with initialization time $\Oh_k(n)$, worst-case update time $\Oh_k(\log \log n)$, and query time $\Oh(1)$.
\end{corollary}

 Note here that the query time can be a constant independent of $k$, as we can always recompute the answer to the query following every update.

For {\sc{Edit Distance}}, the formula is more complicated. For two words $u,v\in \Sigma^\star$, by $u\otimes v$ we denote the word over $(\Sigma\cup \{\bot\})^2$, where $\bot$ is a symbol not present in $\Sigma$, defined as follows:
\begin{itemize}
 \item The length of $u\otimes v$ is $\max(|u|,|v|)$.
 \item For each $1\leq i\leq \min(|u|,|v|)$, we put $u\otimes v[i]=(u[i],v[i])$.
 \item For each $\min(|u|,|v|)<i\leq \max(|u|,|v|)$, we put $u\otimes v[i]=(u[i],\bot)$ or $u\otimes v[i]=(\bot,v[i])$, depending on whether $\max(|u|,|v|)=|u|$ or $\max(|u|,|v|)=|v|$.
\end{itemize}

\begin{lemma}\label{lem:ed-sentence}
 Let $k\in \N$ and $\Sigma$ be a finite alphabet.
 There is a sentence $\psi_{k,\Sigma}\in \FO[(\Sigma\cup \{\bot\})^2,<]$, computable from $k$ and $\Sigma$, such that for all $u,v\in \Sigma^\star$, we have $\ED(u,v)\leq k$ if and only if $u\otimes v\models \psi_{k,\Sigma}$.
\end{lemma}
\begin{proof}
 Denote $\Gamma=(\Sigma\cup \{\bot\})^2$ for brevity.
 Note that for two words $u',v'\in \Sigma^\star$ we have $\ED(u',v')\leq k$ if and only if there exist integers $a,b,c\geq 0$ with $a+b+c\leq k$ such that  one can remove $a$ positions from $u'$ and $b$ positions from $v'$ so that the resulting strings have equal length and differ on exactly $c$ positions.
 In such case, we will call the pair $(u',v')$ {\em{$(a,b,c)$-editable}}.
 
 For all $(a,b,c)\in \{0,1,\ldots,k\}^3$ with $a+b+c\leq k$ and $s,t\in \{-k,\ldots,k\}$, we shall construct a formula $\alpha_{s,t,a,b,c}(x,y)$ that satisfies the following: for two positions $1\leq x\leq y\leq \max(|u|,|v|)$, we have
 $$u\otimes v\models \alpha_{s,t,a,b,c}(x,y)\qquad\textrm{if and only if}\qquad  (u[x : y],v[x+s : y+t])\textrm{ is }(a,b,c)\textrm{-editable.}$$
 Here, we use the convention that if the specified range $[x : y]$ or $[x+s : y+t]$ does not fit into the corresponding word, or makes no sense due to $x>y+1$ or $x+s>y+t+1$, then $\alpha_{s,t,a,b,c}(x,y)$ should be false (this can be easily recognized in $\FO[\Gamma,<]$). If we achieve the above, the formula $\psi_{k,\Sigma}$ can be defined as the disjunction of all formulas $\alpha_{0,t,a,b,c}(1,n_u)$ for $a,b,c$ as above, where $1$ is the first position, $n_u$ is the last position of $u$, and $t\in \{-k,\ldots,k\}$ is such that $n_u+t$ is the last position of~$v$ (all these are easily definable from $u\otimes v$ in $\FO[\Gamma,<]$).
 
 The construction is by induction on $a+b$. For the base case $a=b=0$, we may
 define $\alpha_{s,t,0,0,c}(x,y)$ as follows: if $s\neq t$ then the formula is
 always false, and otherwise it checks whether there are exactly $c$ different
 positions $z$ such that $x\leq z\leq y$ and $u[z]\neq v[z+s]$. This can be
 checked by comparing the first coordinate of $u\otimes v[z]$ with the second
 coordinate of $u\otimes v[z+s]$. Since $|s|\leq k$ by assumption, it is straightforward to formulate this assertion in $\FO[\Gamma,<]$.
 
 We proceed to the induction step. So assume $a+b>0$, say $a>0$; the construction in the case $b>0$ is analogous, so we omit it. The idea is that we guess, by existential quantification, the first position in $u[x : y]$ that gets removed, and use simpler formulas given by the induction assumption. More precisely, $\alpha_{s,t,a,b,c}(x,y)$ can be defined as the conjunction of formulas
 $$\exists z.\left(x\leq z\leq y \wedge \alpha_{s,r,0,b_1,c_1}(x,z-1)\wedge \alpha_{r-1,t,a-1,b_2,c_2}(z+1,y)\right),$$
 for all integers $b_1,b_2\geq 0$ with $b_1+b_2=b$, $c_1,c_2\geq 0$ with $c_1+c_2=c$, and $r\in \{-k+1,\ldots,k\}$. Here, $z-1$ and $z+1$ are a syntactic sugar for the predecessor and the successor of $z$, respectively, which are easily definable in $\FO[\Gamma,<]$.
 It is straightforward to see that the construction of $\alpha_{s,t,a,b,c}(x,y)$ as above satisfies the required properties.
\end{proof}

Similarly as before, by combining Theorem~\ref{thm:meta} with Lemma~\ref{lem:ed-sentence} we obtain the following.

\begin{corollary}
 There is a data structure for the dynamic {\sc{Edit Distance}} problem with initialization time $\Oh_{k,\Sigma}(n)$, worst-case update time $\Oh_{k,\Sigma}(\log \log n)$, and query time $\Oh(1)$. 
\end{corollary}

