\chapter{Preliminaries}\label{r:prelim}
\label{sec:prelim}

For a parameter $\ell$, we write $\Oh_\ell(\cdot)$ to hide factors depending
only on $\ell$. By $\poly(n_1,n_2)$ we denote $(n_1n_2)^{\Oh(1)}$.  We use a
shorthand notation $[n] \coloneqq \{1,\ldots,n\}$. For two sets $X,Y$, $X
\triangle Y$ denotes their symmetric difference $(X \setminus Y) \cup (Y
\setminus X)$.  For two words $u,v \in \Sigma^L$, by $\dist(u,v)$ we denote the
Hamming distance between $u$ and $v$. For a word $u \in \Sigma^L$ and a set $X
\subseteq [L]$, we write $a[X] \in \Sigma^{|X|}$ for the word obtained from $u$
by removing all positions outside of $X$.  For $1 \leq i \leq j \leq m$, we
write $u[i:j] \in \Sigma^{j-i+1}$ for $u[\{i,\ldots,j\}]$.

In this paper, we work in the standard word-RAM model, a computation model in which a random-access machine does bitwise operations on a word of $w$ bits.
The following {\sc{Predecessor}} problem is a crucial building stone of our algorithm.

\defproblem{{\sc{Predecessor}}}{Maintain a set $S \subseteq [2^w]$ and $|S| \leq n$}{For a given $x \in [2^w]$ return $\max \{y : y \in S \land x \geq y\}$}

In all our results the
$\Oh(\log\log n )$ factors come exclusively from the application of van Emde
Boas trees that solve {\sc{Predecessor}} problem. {\sc{Predecessor}} problem is a well-studied problem
both in terms of lower and upper bounds (see the recent survey~\cite{NavarroR20}).
In word-RAM, the complexity of {\sc{Predecessor}} operations is well understood
to be

\begin{equation}
    \label{eq:pred}
    \Theta\left(
        \max \left[1,
            \min \left\{
                \log_w(n),
                \frac{\log\frac{w}{\log{w}}}{\log\left(\log\frac{w}{\log{w}}/\log
                \frac{\log n}{\log w}\right)},
                \log \frac{\log(2^w-n)}{\log w}
            \right\}
        \right]
    \right)
\end{equation}

The upper and lower bounds were given by P{\u{a}}tra{\c{s}}cu and
Thorup~\cite{PatrascuT14}, see also~\cite{BeameF02,FredmanW93}.
Strictly speaking, $\Oh(\log\log n )$ factors in our paper could be replaced with Equation~\ref{eq:pred}.
We are using the worse $\Oh(\log\log n )$ to keep the results transparent.
Note that the $\Oh(\log\log n )$ bound for {\sc{Predecessor}} is tight in more restricted computation models, see e.g.~\cite{MehlhornNA88}.
